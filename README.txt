
Hide Nodetitle Module
------------------------
by ts145nera, ts145nera@hotmail.com


Description
-----------
This module permits you disable nodetitle of content type. By default this
module set the nodetitle to "Undefined", but the really purpose of this 
module is use another module to generate automatically the nodetitle as "Rules"

Installation 
------------

 * Copy the module's directory to your modules directory and activate the 
 module.
 * For each content type you can enable or disable nodetitle
