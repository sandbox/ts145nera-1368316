(function ($) {

Drupal.behaviors.auto_nodetitleFieldsetSummaries = {
  attach: function (context) {
    $('fieldset#edit-hide-nodetitle', context).drupalSetSummary(function (context) {

      // Retrieve the value of the selected radio button
      var hnt = $("input[@name=#edit-hide-nodetitle-hnt]:checked").val();

      if (hnt == 0) {
        return Drupal.t('Show');
      }
      else if (hnt == 1) {
        return Drupal.t('Hide (hide title field)');
      }
    });
  }
};

})(jQuery);
